import { ModuleWithProviders } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Componentes
import { HomeComponent } from './home/home.component';
import { UserPostsComponent } from './user-posts/user-posts.component';
import { PostComponent } from './post/post.component';
import { PostNewComponent } from './post-new/post-new.component';
import { PostEditComponent } from './post-edit/post-edit.component';

// Se definen las rutas
const appRoutes: Routes = [
    {path:'', component: HomeComponent},
    {path:'home', component: HomeComponent},
    {path:'user/:id', component: UserPostsComponent},
    {path:'post/:id', component: PostComponent},
    {path:'post-new', component: PostNewComponent},
    {path:'post-edit/:id', component: PostEditComponent},
    {path:'**', component: HomeComponent},
    
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);