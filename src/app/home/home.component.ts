import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [PostService]
})
export class HomeComponent implements OnInit {
  public title_page: string;
  public posts;
  public users;
  constructor(
    private _postService: PostService,
    private _router: Router 
  )
  {
    this.title_page = "Artículos";  
  }

  ngOnInit() {
    this.getPosts();
    this.getUsers();
    
  }
  //Función para filtro de usuarios, en el parámetro "event" es donde viene el id del user que luego utilizamos para filtrar los artículos
  userPosts(event){
    //console.log(event);
    if (event == "todos"){
      this._router.navigate(['/']);
    }else{
      this._router.navigate(['/user/' + event]);
    }
  }
  //Función para obtener todos los artículos
  getPosts(){
    this._postService.getPosts().subscribe(
      result => {
        this.posts = result;  
        if(!this.posts){
            console.log("Error en el servidor");
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
      }
    );
  }
  //Función para obtener todos los usuarios
  getUsers(){
    this._postService.getUsers().subscribe(
      result => {
        this.users = result; 
        if(!this.users){
            console.log("Error en el servidor");
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
      }
    );
  }
  //Función para eliminar artículos
  deletePost(id){
    this._postService.delete(id).subscribe(
      result => {
        this.getPosts();
        console.log("Artículo id= " + id + " eliminado correctamente");
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  
  
}
