import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  providers: [PostService]
})
export class PostComponent implements OnInit {
  public post;
  public comments;

  constructor(
    private _postService: PostService,
    private _route: ActivatedRoute,
    private _router: Router 
  ) { }

  ngOnInit() {
    this.getPost();
    this.getComments();
  }
  
  getPost(){
    //Se recibe el parámetro id que viene en la url
    this._route.params.forEach((params: Params) => {
      let id = params['id'];
      //Se hace la consulta al servicio
      this._postService.getPost(id).subscribe(
        result => {
          this.post = result;
          if(!this.post){
            console.log("Error en el servidor");
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }
  
  //Recibir comentarios
  getComments(){
    //Se recibe el parámetro id que viene en la url
    this._route.params.forEach((params: Params) => {
      let id = params['id'];
      //Se hace la consulta al servicio
      this._postService.getComments(id).subscribe(
        result => {
          this.comments = result;
          if(!this.comments){
            console.log("Error en el servidor");
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }

}
