import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PostService } from '../services/post.service';
import { Post } from '../models/post';

@Component({
  selector: 'app-post-new',
  templateUrl: './post-new.component.html',
  providers: [PostService]
})
export class PostNewComponent implements OnInit {
  public title_page: string;
  public post:Post;
  public users;
  //La variable "status" se usará para mostrar un mensaje de confirmación en la vista
  public status: string;
  constructor(
    private _postService: PostService,
    private _route: ActivatedRoute,
    private _router: Router
  ) 
  {
    this.title_page = "Agregar un nuevo artículo";
  }

  ngOnInit() {
    //Creamos el objeto del artículo
    this.post = new Post(1, 1, '', '');
    
    this.getUsers();
  }
  //Se reciben los datos del formulario
  onSubmit(form){
    this._postService.create(this.post).subscribe(
      result => {
        console.log(result);
        this.status = 'success';
      },
      error => {
        console.log(<any>error); 
        this.status = 'error';        
      }
    );
  }
  
  //Obtenemos todos los usuarios que luego usaremos en el input select del formulario
  getUsers(){
    this._postService.getUsers().subscribe(
      result => {
        this.users = result;


        if(!this.users){
            console.log("Error en el servidor");
        }
      },
      error => {
        console.log(<any>error);
      }

    );
  }

}
