import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { map } from 'rxjs/operators';
import {Observable} from 'rxjs';
import { GLOBAL } from './global';
import { Post } from '../models/post';

@Injectable()
export class PostService {
public url: string;

  constructor(
    private _http:Http
  ){  
    this.url = GLOBAL.url;    
  }

  // Obtener todos los artículos 
  getPosts(){
    return this._http.get(this.url + 'posts')
               .pipe(map(res => res.json())); 
  }
  // Obtener todos los usuarios
  getUsers(){
    return this._http.get(this.url + 'users')
               .pipe(map(res => res.json())); 
  }
  // Obtener todos los artículos de un usuario
  getUserPosts(id){
    return this._http.get(this.url + 'posts?userId=' + id)
               .pipe(map(res => res.json())); 
  }
  // Obtener datos de un usuario
  getUser(id){
    return this._http.get(this.url + 'users/' + id)
               .pipe(map(res => res.json())); 
  }
  // Obtener datos de un artículo
  getPost(id){
    return this._http.get(this.url + 'posts/' + id)
               .pipe(map(res => res.json())); 
  }
  // Obtener comentarios
  getComments(id){
    return this._http.get(this.url + 'posts/' + id + '/comments')
               .pipe(map(res => res.json())); 
  }
  // Crear un nuevo artículo
  create(post: Post): Observable<any>{
    let json = JSON.stringify(post);
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let params = 'json='+json;
    return this._http.post(this.url+'posts', params, {headers: headers})
                     .pipe(map(res => res.json()));;
  }
  // Editar un nuevo artículo
  update(id, post: Post): Observable<any>{
    let json = JSON.stringify(post);
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let params = 'json='+json;
    return this._http.put(this.url+'posts/' + id, params, {headers: headers})
                     .pipe(map(res => res.json()));;
  }
  // Eliminar producto
  delete(id){
    return this._http.delete(this.url + 'posts/' + id)
               .pipe(map(res => res.json())); 
  }

}