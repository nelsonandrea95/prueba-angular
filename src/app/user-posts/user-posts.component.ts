import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-user-posts',
  templateUrl: '../home/home.component.html',
  providers: [PostService]
})
export class UserPostsComponent implements OnInit {
  public title_page: string;
  public posts;
  public users;
  public user;
  
  constructor(
    private _postService: PostService,
    private _route: ActivatedRoute,
    private _router: Router 
  )
  {
    this.title_page = "Artículos de ";
  }

  ngOnInit() {
    this.getPosts();
    this.getUser();
    this.getUsers();
  }
  
  getPosts(){
    //Se recibe el parámetro id que viene en la url
    this._route.params.forEach((params: Params) => {
      let id = params['id'];
      //Se hace la consulta al servicio
      this._postService.getUserPosts(id).subscribe(
        result => {
          this.posts = result;
          console.log(result);
          if(!this.posts){
            console.log("Error en el servidor");
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }
  
  //Con esta función conseguimos los datos del usuario que luego usaremos en la vista
  getUser(){
    //Se recibe el parámetro id que viene en la url
    this._route.params.forEach((params: Params) => {
      let id = params['id'];
      //Se hace la consulta al servicio
      this._postService.getUser(id).subscribe(
        result => {
          this.user = result;
          console.log(result);
          if(!this.user){
            console.log("Error en el servidor");
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }
  
  getUsers(){
    this._postService.getUsers().subscribe(
      result => {
        this.users = result;
        console.log(result);


        if(!this.users){
            console.log("Error en el servidor");
        }
      },
      error => {
        console.log(<any>error);
      }

  );
  }
  
  userPosts(event){
    //console.log(event);
    if (event == "todos"){
      this._router.navigate(['/']);
    }else{
      this._router.navigate(['/user/' + event]);
    }
  }
  
  
  
  

}
