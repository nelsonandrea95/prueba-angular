import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PostService } from '../services/post.service';
import { Post } from '../models/post';

@Component({
  selector: 'app-post-edit',
  templateUrl: '../post-new/post-new.component.html',
  providers: [PostService]
})
export class PostEditComponent implements OnInit {
  public title_page: string;
  public is_edit: boolean;
  public post : Post;
  public status : string;
  public users;
  constructor(
    private _postService: PostService,
    private _route: ActivatedRoute,
    private _router: Router
  ) 
  {
    this.title_page = "Editar Artículo";
    this.is_edit = true; 
  }

  ngOnInit() {
    this.post = new Post(1, 1, '', '');
    this.getPost();
    this.getUsers();
  }
  
  getPost(){
    //Se recibe el parámetro id que viene en la url
    this._route.params.forEach((params: Params) => {
      let id = params['id'];
      //Se hace la consulta al servicio
      this._postService.getPost(id).subscribe(
        result => {
          this.post = result;
          if(!this.post){
            this._router.navigate(['/']);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }
  
  //Obtenemos todos los usuarios que luego usaremos en el input select del formulario
  getUsers(){
    this._postService.getUsers().subscribe(
      result => {
        this.users = result;


        if(!this.users){
            console.log("Error en el servidor");
        }
      },
      error => {
        console.log(<any>error);
      }

    );
  }
  
  onSubmit(){
    this._route.params.forEach((params: Params) => {
      let id = params['id'];
      this._postService.update(id, this.post).subscribe(
        result => {
          console.log(result);
          this.status = 'success';
          this._router.navigate(['/post', id]);
        },
        error => {
          console.log(<any>error); 
          this.status = 'error';        
        }
      );
    });
  }

}
